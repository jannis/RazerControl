import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.6.21"
    id("maven-publish")
}

val razerControlVersion = "0.3.0"

group = "de.tadris.razer"
version = razerControlVersion
description = "$name is a simple Java library written in Kotlin for controlling Razer devices via OpenRazer."
apply("local.gradle")

repositories {
    mavenCentral()
}

dependencies {
    testImplementation(kotlin("test"))
}

tasks.test {
    useJUnitPlatform()
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}

publishing {
    repositories {
        maven {
            setUrl(project.ext["mavenUrl"]!!)
            credentials {
                username = project.ext["mavenUser"] as String?
                password = project.ext["mavenPassword"] as String?
            }
        }
    }
    publications {
        create<MavenPublication>("razer-control"){
            groupId = "de.tadris.razer"
            artifactId = "razer-control"
            version = razerControlVersion
            from(components["java"])

            pom {
                name.set("Razer Control")
                description.set("A simple kotlin/java library for controlling Razer devices via openrazer")
            }
        }
    }
}