package de.tadris.razer.api

import de.tadris.razer.model.StringValue
import de.tadris.razer.device.RazerDevice
import kotlin.reflect.KProperty

/**
 * Helper to read and write string values
 */
class StringAPI(private val key: String) {

    operator fun getValue(api: RazerDevice, property: KProperty<*>) = api.read(key)

    operator fun setValue(api: RazerDevice, property: KProperty<*>, str: String) {
        api.runWrite(key, StringValue(str))
    }


}