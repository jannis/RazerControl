package de.tadris.razer.api

import de.tadris.razer.device.DeviceType
import de.tadris.razer.device.Keyboard
import de.tadris.razer.device.Mouse
import de.tadris.razer.device.RazerDevice
import java.io.File

/**
 * Scans for devices and holds a reference to them.
 */
class RazerAPI(val debug: Boolean = false) {

    companion object {
        val deviceParents = mapOf(
            "/sys/bus/hid/drivers/razermouse" to DeviceType.MOUSE,
            "/sys/bus/hid/drivers/razerkbd" to DeviceType.KEYBOARD,
        )
    }

    val devices: Map<DeviceType, RazerDevice> =
        deviceParents.mapNotNull { (path, type) ->
            val files = File(path).listFiles() ?: return@mapNotNull null
            val dir = files.find { directory -> directory.isDirectory && directory.listFiles()?.find { it.name == "device_type" } != null } ?: return@mapNotNull null
            type to createFromDir(type, dir)
        }.toMap()

    private fun createFromDir(type: DeviceType, directory: File) = when(type){
        DeviceType.MOUSE -> Mouse(directory, debug)
        DeviceType.KEYBOARD -> Keyboard(directory, debug)
    }

    fun mouse() = devices[DeviceType.MOUSE] as? Mouse

    fun keyboard() = devices[DeviceType.KEYBOARD] as? Keyboard

}