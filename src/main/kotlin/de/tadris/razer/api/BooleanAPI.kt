package de.tadris.razer.api

import de.tadris.razer.model.StringValue
import de.tadris.razer.device.Mouse
import de.tadris.razer.device.RazerDevice
import kotlin.reflect.KProperty

/**
 * Helper to read and write boolean values
 */
class BooleanAPI(private val key: String) {

    operator fun getValue(api: RazerDevice, property: KProperty<*>) = api.read(key) == "1"

    operator fun setValue(api: RazerDevice, property: KProperty<*>, value: Boolean) {
        api.runWrite(key, StringValue(if(value) "1" else "0"))
    }


}