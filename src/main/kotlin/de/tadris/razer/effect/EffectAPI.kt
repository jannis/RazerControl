package de.tadris.razer.effect

import de.tadris.razer.api.RazerAPI
import de.tadris.razer.matrix.KeyboardMatrix

class EffectAPI(val api: RazerAPI) {

    val matrix = api.keyboard()?.let { KeyboardMatrix(it) }

}