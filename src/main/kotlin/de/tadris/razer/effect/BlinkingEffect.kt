package de.tadris.razer.effect

import de.tadris.razer.matrix.MatrixLocation

class BlinkingEffect(val keys: List<MatrixLocation>, val color: Int, interval: Int) : Effect() {

    constructor(key: MatrixLocation, color: Int, interval: Int): this(listOf(key), color, interval)

    private var isOn = true
    private val interval = EffectInterval(interval)

    override fun onAdd() {
        update()
    }

    override fun tick() {
        if(interval.check()){
            isOn = !isOn
            update()
        }
    }

    override fun onRemove() {
        isOn = false
        update()
    }

    fun update(){
        if(isOn){
            api.matrix?.write(keys, color)
        }else{
            api.matrix?.clear(keys)
        }
    }
}