package de.tadris.razer.effect

class EffectInterval(val millis: Int) {

    private var lastTick = System.currentTimeMillis()

    fun check() = if(System.currentTimeMillis() - lastTick > millis){
        lastTick = System.currentTimeMillis()
        true
    }else false

}