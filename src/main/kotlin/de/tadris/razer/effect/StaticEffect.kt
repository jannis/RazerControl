package de.tadris.razer.effect

import de.tadris.razer.matrix.MatrixLocation

class StaticEffect(private val keys: List<MatrixLocation>, private val color: Int): Effect() {

    constructor(key: MatrixLocation, color: Int): this(listOf(key), color)

    override fun onAdd() {
        api.matrix?.write(keys, color)
    }

    override fun tick() { }

    override fun onRemove() {
        api.matrix?.clear(keys)
    }
}