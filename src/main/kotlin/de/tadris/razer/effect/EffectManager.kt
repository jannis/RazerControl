package de.tadris.razer.effect

import de.tadris.razer.api.RazerAPI
import java.util.concurrent.Executors

class EffectManager(val api: RazerAPI, tickDelay: Int = DEFAULT_TICK_DELAY) {

    companion object {
        const val DEFAULT_TICK_DELAY = 50
    }

    var active = true

    private val tickInterval = EffectInterval(tickDelay)

    private val effectAPI = EffectAPI(api)
    private val activeEffects = mutableListOf<Effect>()
    val executor = Executors.newSingleThreadExecutor()

    fun add(effect: Effect){
        effect.init(effectAPI)
        effect.onAdd()
        activeEffects.add(effect)
    }

    fun removeAll(){
        activeEffects.toList().forEach { remove(it) }
    }

    fun remove(effect: Effect){
        effect.onRemove()
        activeEffects.remove(effect)
    }

    fun tick(){
        if(!active) return
        if(tickInterval.check()){
            activeEffects.forEach {
                it.tick()
            }
            activeEffects.filter { it.removeRequested }.forEach {
                remove(it)
            }
            executor.submit { effectAPI.matrix?.flush() }
        }
    }

    fun destroy(){
        removeAll()
        executor.shutdownNow()
    }

}