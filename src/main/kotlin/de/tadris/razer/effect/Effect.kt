package de.tadris.razer.effect

abstract class Effect {

    var removeRequested = false
        private set
    protected lateinit var api: EffectAPI

    fun init(api: EffectAPI){
        this.api = api
    }

    fun remove(){
        removeRequested = true
    }

    abstract fun onAdd()

    abstract fun tick()

    abstract fun onRemove()

}