package de.tadris.razer.effect

class FlashEffect(private val color: Int, duration: Int) : Effect() {

    private var interval = EffectInterval(duration)

    override fun onAdd() {
        api.matrix?.apply {
            writeBuffer = true
            fill(color)
            writeBuffer = false
            displayedBuffer = true
        }
    }

    override fun tick() {
        if(interval.check()){
            remove()
        }
    }

    override fun onRemove() {
        api.matrix?.apply {
            writeBuffer = true
            clearAll()
            writeBuffer = false
            displayedBuffer = false
        }
    }
}