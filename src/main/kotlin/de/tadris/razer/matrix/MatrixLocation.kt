package de.tadris.razer.matrix

data class MatrixLocation(val x: Int, val y: Int)