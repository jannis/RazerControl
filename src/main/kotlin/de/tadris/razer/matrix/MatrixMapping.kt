package de.tadris.razer.matrix

class MatrixMapping(layout: String) {

    companion object {

        val MAPPING = mapOf(
            7 to MatrixLocation(11, 1), // NUM_0
            8 to MatrixLocation(2, 1), // NUM_1
            9 to MatrixLocation(3, 1), // NUM_2
            10 to MatrixLocation(4, 1), // NUM_3
            11 to MatrixLocation(5, 1), // NUM_4
            12 to MatrixLocation(6, 1), // NUM_5
            13 to MatrixLocation(7, 1), // NUM_6
            14 to MatrixLocation(8, 1), // NUM_7
            15 to MatrixLocation(9, 1), // NUM_8
            16 to MatrixLocation(10, 1), // NUM_9
            29 to MatrixLocation(2, 3), // A
            57 to MatrixLocation(3, 5), // ALT_LEFT
            58 to MatrixLocation(11, 5), // ALT_RIGHT
            75 to MatrixLocation(12, 3), // APOSTROPHE
            30 to MatrixLocation(7, 4), // B
            73 to MatrixLocation(2, 4), // BACKSLASH
            31 to MatrixLocation(5, 4), // C
            115 to MatrixLocation(1, 3), // CAPS_LOCK
            55 to MatrixLocation(10, 4), // COMMA
            32 to MatrixLocation(4, 3), // D
            67 to MatrixLocation(14, 1), // BACKSPACE
            112 to MatrixLocation(15, 2), // FORWARD_DEL
            23 to MatrixLocation(19, 3), // DPAD_CENTER
            20 to MatrixLocation(19, 4), // DPAD_DOWN
            21 to MatrixLocation(18, 3), // DPAD_LEFT
            22 to MatrixLocation(20, 3), // DPAD_RIGHT
            19 to MatrixLocation(19, 2), // DPAD_UP
            20 to MatrixLocation(16, 5), // DOWN
            21 to MatrixLocation(15, 5), // LEFT
            22 to MatrixLocation(17, 5), // RIGHT
            19 to MatrixLocation(16, 4), // UP
            33 to MatrixLocation(4, 2), // E
            66 to MatrixLocation(14, 3), // ENTER
            70 to MatrixLocation(13, 1), // EQUALS
            34 to MatrixLocation(5, 3), // F
            35 to MatrixLocation(6, 3), // G
            36 to MatrixLocation(7, 3), // H
            37 to MatrixLocation(9, 2), // I
            38 to MatrixLocation(8, 3), // J
            39 to MatrixLocation(9, 3), // K
            40 to MatrixLocation(10, 3), // L
            71 to MatrixLocation(12, 2), // LEFT_BRACKET
            41 to MatrixLocation(9, 4), // M
            69 to MatrixLocation(12, 1), // MINUS
            42 to MatrixLocation(8, 4), // N
            43 to MatrixLocation(10, 2), // O
            44 to MatrixLocation(11, 2), // P
            121 to MatrixLocation(17, 0), // PAUSE
            56 to MatrixLocation(11, 4), // PERIOD
            81 to MatrixLocation(13, 2), // PLUS
            120 to MatrixLocation(15, 0), // PRINT_SCREEN
            45 to MatrixLocation(2, 2), // Q
            46 to MatrixLocation(5, 2), // R
            72 to MatrixLocation(13, 2), // RIGHT_BRACKET
            47 to MatrixLocation(3, 3), // S
            116 to MatrixLocation(16, 0), // SCROLL_LOCK
            74 to MatrixLocation(11, 3), // SEMICOLON
            59 to MatrixLocation(1, 4), // SHIFT_LEFT
            60 to MatrixLocation(14, 4), // SHIFT_RIGHT
            76 to MatrixLocation(12, 4), // SLASH
            62 to MatrixLocation(7, 5), // SPACE
            48 to MatrixLocation(6, 2), // T
            61 to MatrixLocation(1, 2), // TAB
            49 to MatrixLocation(8, 2), // U
            50 to MatrixLocation(6, 4), // V
            51 to MatrixLocation(3, 2), // W
            52 to MatrixLocation(4, 4), // X
            53 to MatrixLocation(3, 4), // Y
            54 to MatrixLocation(7, 2), // Z
            129 to MatrixLocation(1, 5), // CONTROL_LEFT
            130 to MatrixLocation(14, 5), // CONTROL_RIGHT
            111 to MatrixLocation(1, 0), // ESCAPE
            123 to MatrixLocation(16, 2), // END
            124 to MatrixLocation(15, 1), // INSERT
            3 to MatrixLocation(16, 1), // HOME
            92 to MatrixLocation(17, 1), // PAGE_UP
            93 to MatrixLocation(17, 2), // PAGE_DOWN
            144 to MatrixLocation(19, 5), // NUMPAD_0
            145 to MatrixLocation(18, 4), // NUMPAD_1
            146 to MatrixLocation(19, 4), // NUMPAD_2
            147 to MatrixLocation(20, 4), // NUMPAD_3
            148 to MatrixLocation(18, 3), // NUMPAD_4
            149 to MatrixLocation(19, 3), // NUMPAD_5
            150 to MatrixLocation(20, 3), // NUMPAD_6
            151 to MatrixLocation(18, 2), // NUMPAD_7
            152 to MatrixLocation(19, 2), // NUMPAD_8
            153 to MatrixLocation(20, 2), // NUMPAD_9
            154 to MatrixLocation(19, 1), // NUMPAD_DIVIDE
            155 to MatrixLocation(20, 1), // NUMPAD_MULTIPLY
            156 to MatrixLocation(21, 1), // NUMPAD_SUBTRACT
            157 to MatrixLocation(21, 2), // NUMPAD_ADD
            159 to MatrixLocation(20, 5), // NUMPAD_COMMA
            160 to MatrixLocation(21, 4), // NUMPAD_ENTER
            143 to MatrixLocation(18, 1), // NUM_LOCK
            73 to MatrixLocation(13, 3), // #
            68 to MatrixLocation(1, 1),
            131 to MatrixLocation(3, 0), // F1
            132 to MatrixLocation(4, 0), // F2
            133 to MatrixLocation(5, 0), // F3
            134 to MatrixLocation(6, 0), // F4
            135 to MatrixLocation(7, 0), // F5
            136 to MatrixLocation(8, 0), // F6
            137 to MatrixLocation(9, 0), // F7
            138 to MatrixLocation(10, 0), // F8
            139 to MatrixLocation(11, 0), // F9
            140 to MatrixLocation(12, 0), // F10
            141 to MatrixLocation(13, 0), // F11
            142 to MatrixLocation(14, 0), // F12
            82 to MatrixLocation(13, 5), // MENU
        )

    }

    private val mapping = MAPPING

    operator fun get(keycode: Int) = mapping[keycode] ?: MatrixLocation(-1, -1)

}