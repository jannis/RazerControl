package de.tadris.razer.matrix

import de.tadris.razer.device.Keyboard
import de.tadris.razer.model.KeyboardFrame

class KeyboardMatrix(private val device: Keyboard) {

    val xBounds = 0 until KeyboardFrame.COLUMNS
    val yBounds = 0 until KeyboardFrame.ROWS

    var backgroundColor = 0x0
    var writeBuffer = false
    var displayedBuffer = false
        set(value) {
            field = value
            setUpdatedAll()
        }

    private val buffer1 = IntArray(KeyboardFrame.CELLS)
    private val buffer2 = IntArray(KeyboardFrame.CELLS)

    private val updatedRows = BooleanArray(KeyboardFrame.ROWS)

    private fun getPos(location: MatrixLocation) = getPos(location.x, location.y)

    private fun getPos(x: Int, y: Int): Int {
        if(x !in xBounds) throw IllegalArgumentException("x out of bounds: $x")
        if(y !in yBounds) throw IllegalArgumentException("y out of bounds: $y")
        return y * KeyboardFrame.COLUMNS + x
    }

    init {
        setUpdatedAll()
    }

    fun clearAll(){
        fill(backgroundColor)
    }

    fun clear(locations: List<MatrixLocation>){
        write(locations, backgroundColor)
    }

    fun clear(location: MatrixLocation){
        write(location, backgroundColor)
    }

    fun fill(color: Int){
        for(x in xBounds){
            for(y in yBounds){
                write(x, y, color)
            }
        }
    }

    /**
     * Writes a multiple key colors
     */
    fun write(locations: List<MatrixLocation>, color: Int){
        locations.forEach { write(it, color) }
    }

    /**
     * Writes a single key color at location
     */
    fun write(location: MatrixLocation, color: Int){
        if(location.x == -1) return // none
        write(location.x, location.y, color)
    }

    /**
     * Writes a single key color at x, y
     */
    fun write(x: Int, y: Int, color: Int){
        val buffer = if(writeBuffer) buffer2 else buffer1
        val pos = getPos(x, y)
        val currentColor = buffer[pos]
        if(color != currentColor){
            buffer[pos] = color
            updatedRows[y] = true
        }
    }

    private fun setUpdatedAll(){
        updatedRows.indices.forEach { updatedRows[it] = true }
    }

    /**
     * Sends changes to the keyboard
     */
    fun flush(){
        val buffer = if(displayedBuffer) buffer2 else buffer1
        updatedRows.forEachIndexed { index, updated ->
            if(updated){
                val colors = buffer.copyOfRange(index * KeyboardFrame.COLUMNS, (index + 1) * KeyboardFrame.COLUMNS)
                device.sendCustomFrameUpdate(KeyboardFrame.FrameUpdate(index, 0, KeyboardFrame.COLUMNS - 1, colors))
                updatedRows[index] = false
            }
        }
    }

}