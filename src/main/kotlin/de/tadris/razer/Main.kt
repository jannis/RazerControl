package de.tadris.razer

import de.tadris.razer.api.RazerAPI
import de.tadris.razer.effect.BlinkingEffect
import de.tadris.razer.effect.EffectManager
import de.tadris.razer.effect.FlashEffect
import de.tadris.razer.effect.StaticEffect
import de.tadris.razer.matrix.KeyboardMatrix
import de.tadris.razer.matrix.MatrixLocation
import de.tadris.razer.matrix.MatrixMapping
import de.tadris.razer.model.KeyboardFrame
import de.tadris.razer.model.WaveDirection
import kotlin.math.sin
import kotlin.random.Random

fun main(){

    val api = RazerAPI()
    println("Found devices:")
    api.devices.forEach {
        println("- " + it.value)
    }

    val mouse = api.mouse()
    val keyboard = api.keyboard()

    println("Mouse: $mouse")
    println("Keyboard: $keyboard")

    mouse!!
    keyboard!!

    val manager = EffectManager(api)

    MatrixMapping.MAPPING.forEach { (key, _) ->
        manager.add(StaticEffect(keyboard.mapping[key], 0xffffff))
    }

    while (true){
        manager.tick()
        Thread.sleep(10)
    }

}