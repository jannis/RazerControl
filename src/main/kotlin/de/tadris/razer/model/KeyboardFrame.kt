package de.tadris.razer.model

class KeyboardFrame(val updates: List<FrameUpdate>) {

    companion object {
        const val ROWS = 6
        const val COLUMNS = 22
        const val CELLS = ROWS * COLUMNS

        fun fromColor(color: Int) = fromColors(
            buildList { repeat(ROWS * COLUMNS){ add(color) } }
        )

        inline fun fromBuilder(builder: (x: Int, y: Int) -> Int) =
            fromRows(buildList {
                repeat(ROWS){ y ->
                    add(buildList {
                        repeat(COLUMNS){ x ->
                            add(builder(x, y))
                        }
                    })
                }
            })

        fun fromColors(colors: List<Int>) = KeyboardFrame(
            buildList {
                repeat(ROWS){ rowIndex ->
                    add(FrameUpdate(rowIndex, 0, COLUMNS - 1, colors.subList(rowIndex * COLUMNS, (rowIndex + 1) * COLUMNS).toIntArray()))
                }
            }
        )

        fun fromRows(rows: List<List<Int>>) = KeyboardFrame(
            rows.mapIndexed { index, update -> FrameUpdate(index, 0, COLUMNS - 1, update.toIntArray()) }
        )
    }

    class FrameUpdate(val rowIndex: Int, val rowStart: Int, val rowStop: Int, val colors: IntArray){

        init {
            if(rowIndex !in 0 until ROWS) throw IllegalArgumentException("Row index out of bounds: $rowIndex")
            if(rowStart !in 0 until COLUMNS) throw IllegalArgumentException("Row start out of bounds: $rowStart")
            if(rowStop !in 0 until COLUMNS) throw IllegalArgumentException("Row stop out of bounds: $rowStop")
            if(rowStop < rowStart) throw IllegalArgumentException("Row stop ($rowStop) is smaller than row start ($rowStart)")
            val expectedColors = rowStop - rowStart + 1
            if(colors.size != expectedColors) throw IllegalArgumentException("Wrong number of colors ($colors), should be $expectedColors for the specified start and stop")
        }

    }

}