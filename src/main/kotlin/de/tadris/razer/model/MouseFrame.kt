package de.tadris.razer.model

/**
 * On the Basilisk V3 a frame has 11 colors: logo, wheel and 9 buttom
 */
class MouseFrame(val colors: IntArray){

    constructor(logo: Int, wheel: Int, bottom: List<Int>): this((listOf(logo, wheel) + bottom).toIntArray())

    constructor(logo: Int, wheel: Int, bottom: Int): this(
        intArrayOf(logo, wheel, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom)
    )

    fun toValue() = ColorValue(colors, byteArrayOf(0, 0, (colors.size - 1).toByte()))

}