package de.tadris.razer.model

enum class WaveDirection(val direction: String) {
    NONE("0"),
    UP("1"),
    DOWN("2")
}