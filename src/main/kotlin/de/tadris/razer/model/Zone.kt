package de.tadris.razer.model

enum class Zone(val id: String) {
    MAIN(""),
    LOGO("logo_"),
    SCROLL("scroll_"),
    MATRIX("matrix_"),
}