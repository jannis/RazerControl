package de.tadris.razer.model

sealed class RazerValue

class StringValue(val content: String): RazerValue(){

    override fun toString() = content

}

open class ColorValue(val argb: IntArray, val prefix: ByteArray = ByteArray(0)): RazerValue() {

    fun toBytes(): ByteArray {
        val array = ByteArray(prefix.size + argb.size * 3)
        prefix.forEachIndexed { index, byte ->
            array[index] = byte
        }
        argb.forEachIndexed { index, color ->
            array[prefix.size + index * 3 + 0] = (color shr 16 and 0xff).toByte()
            array[prefix.size + index * 3 + 1] = (color shr 8 and 0xff).toByte()
            array[prefix.size + index * 3 + 2] = (color and 0xff).toByte()
        }
        return array
    }

    override fun toString() = "(${prefix.contentToString()} | ${argb.contentToString()})"

}

class ByteValue(bytes: ByteArray): ColorValue(intArrayOf(), prefix = bytes)