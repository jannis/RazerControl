package de.tadris.razer.device

import de.tadris.razer.api.BooleanAPI
import de.tadris.razer.api.StringAPI
import de.tadris.razer.model.*
import java.io.File

/**
 * Device representing a mouse
 *
 * could be interesting: https://github.com/openrazer/openrazer/wiki/Using-the-mouse-driver
 */
class Mouse(directory: File, debug: Boolean) : RazerDevice(DeviceType.MOUSE, directory, debug) {

    val version by StringAPI("version")
    val modAlias by StringAPI("modalias")
    var pollRate by StringAPI("poll_rate")
    var dpi by StringAPI("dpi")

    var freeSpin by BooleanAPI("scroll_mode")
    var smartReel by BooleanAPI("scroll_smart_reel")

    /**
     * Brightness between 0 and 255
     */
    fun setBrightness(brightness: Int, zone: Zone = Zone.MAIN){
        val prefix = when(zone){
            Zone.MAIN -> "matrix"
            Zone.LOGO -> "logo_led"
            Zone.SCROLL -> "scroll_led"
            Zone.MATRIX -> "matrix"
        }
        runWrite("${prefix}_brightness", StringValue(brightness.toString()))
    }

    fun setStatic(color: Int, zone: Zone = Zone.MAIN){
        runWrite(zone, "matrix_effect_static", ColorValue(intArrayOf(color)))
    }

    fun setCustomFrame(frame: MouseFrame){
        runWrite("matrix_custom_frame", frame.toValue())
    }

    fun enableCustomAnimation(){
        runWrite("matrix_effect_custom", StringValue("1"))
    }

    fun setSpectrum(zone: Zone = Zone.MAIN){
        runWrite(zone, "matrix_effect_spectrum", StringValue("1"))
    }

    fun setWave(direction: WaveDirection, zone: Zone = Zone.MAIN){
        runWrite(zone, "matrix_effect_wave", StringValue(direction.direction))
    }

    /**
     * Turns off the lights?
     */
    fun setNone(zone: Zone = Zone.MAIN){
        runWrite(zone, "matrix_effect_none", StringValue("1"))
    }
}