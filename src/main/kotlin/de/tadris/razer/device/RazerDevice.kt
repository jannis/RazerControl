package de.tadris.razer.device

import de.tadris.razer.api.StringAPI
import de.tadris.razer.model.ColorValue
import de.tadris.razer.model.RazerValue
import de.tadris.razer.model.StringValue
import de.tadris.razer.model.Zone
import java.io.File

/**
 * A razer device.
 *
 * @property type Device type
 * @property directory File access to the driver
 */
sealed class RazerDevice(val type: DeviceType, val directory: File, val debug: Boolean) {

    /**
     * Gets the serial number from the device, usually 2 letters then 13 digits
     *
     * e.g. XX1111111111111
     */
    val deviceSerial by StringAPI("device_serial")

    /**
     * Product name
     */
    val deviceType by StringAPI("device_type")

    /**
     * Gets the firmware version from the device.
     */
    val firmwareVersion by StringAPI("firmware_version")

    val country by StringAPI("country")

    var deviceMode by StringAPI("device_mode")

    fun runWrite(zone: Zone, key: String, value: RazerValue){
        runWrite("${zone.id}$key", value)
    }

    fun runWrite(key: String, value: RazerValue){
        if(debug) println("Writing $key -> $value")
        val file = File(directory, key)
        when(value){
            is StringValue -> {
                val str = value.content
                file.writeText(str)
            }
            is ColorValue -> {
                val bytes = value.toBytes()
                file.writeBytes(bytes)
            }
        }
    }

    fun read(key: String): String {
        val file = File(directory, key)
        return file.readText().trim()
    }

    override fun toString() = "$deviceType ($deviceSerial)"

}