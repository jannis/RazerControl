package de.tadris.razer.device

import de.tadris.razer.api.BooleanAPI
import de.tadris.razer.api.StringAPI
import de.tadris.razer.matrix.MatrixMapping
import de.tadris.razer.model.*
import de.tadris.razer.model.KeyboardFrame.FrameUpdate
import java.io.File

class Keyboard(directory: File, debug: Boolean) : RazerDevice(DeviceType.KEYBOARD, directory, debug) {

    val mapping by lazy { MatrixMapping(keyboardLayout) }

    var gameLedState by BooleanAPI("game_led_state")

    /**
     * see https://github.com/openrazer/openrazer/wiki/Keyboard-layouts
     */
    val keyboardLayout by StringAPI("kbd_layout")

    /**
     * True means that the macro led is blinking
     */
    var macroLedEffect by BooleanAPI("macro_led_effect")

    /**
     * True means that the macro led is on
     */
    var macroLedState by BooleanAPI("macro_led_state")

    /**
     * Brightness between 0 and 255
     */
    fun setBrightness(brightness: Int){
        runWrite(Zone.MATRIX, "brightness", StringValue(brightness.toString()))
    }

    /**
     * Sets breath effect with random colors
     */
    fun setBreath(){
        runWrite(Zone.MATRIX, "effect_breath", StringValue("1"))
    }

    /**
     * Sets breath effect with one color
     */
    fun setBreath(color: Int){
        runWrite(Zone.MATRIX, "effect_breath", ColorValue(intArrayOf(color)))
    }

    /**
     * Sets breath effect with two colors
     */
    fun setBreath(color1: Int, color2: Int){
        runWrite(Zone.MATRIX, "effect_breath", ColorValue(intArrayOf(color1, color2)))
    }

    fun setStarlight(speed: Int){
        runWrite(Zone.MATRIX, "effect_starlight", ByteValue(byteArrayOf(speed.toByte())))
    }

    fun setStarlight(speed: Int, color: Int){
        runWrite(Zone.MATRIX, "effect_starlight", ColorValue(argb = intArrayOf(color), prefix = byteArrayOf(speed.toByte())))
    }

    fun setStarlight(speed: Int, color1: Int, color2: Int){
        runWrite(Zone.MATRIX, "effect_starlight", ColorValue(argb = intArrayOf(color1, color2), prefix = byteArrayOf(speed.toByte())))
    }

    fun setCustomFrame(frame: KeyboardFrame){
        frame.updates.forEach {
            sendCustomFrameUpdate(it)
        }
        sendCustomFrameFlush()
    }

    fun sendCustomFrameUpdate(update: FrameUpdate){
        runWrite(Zone.MATRIX, "custom_frame", ColorValue(update.colors, prefix = byteArrayOf(
            update.rowIndex.toByte(), update.rowStart.toByte(), update.rowStop.toByte()
        )))
    }

    fun sendCustomFrameFlush(){
        runWrite(Zone.MATRIX, "effect_custom", StringValue("1"))
    }

    fun setNone(){
        runWrite(Zone.MATRIX, "effect_none", StringValue("1"))
    }

    /**
     * Speed is between 1-3
     */
    fun setReactive(color: Int, speed: Int){
        runWrite(Zone.MATRIX, "effect_reactive", ColorValue(prefix = byteArrayOf(speed.toByte()), argb = intArrayOf(color)))
    }

    fun setSpectrum(){
        runWrite(Zone.MATRIX, "effect_spectrum", StringValue("1"))
    }

    fun setStatic(color: Int){
        runWrite(Zone.MATRIX, "effect_static", ColorValue(intArrayOf(color)))
    }

    fun setWave(direction: WaveDirection){
        runWrite(Zone.MATRIX, "effect_wave", StringValue(direction.direction))
    }

    fun setWheel(direction: WaveDirection){
        runWrite(Zone.MATRIX, "effect_wheel", StringValue(direction.direction))
    }

}