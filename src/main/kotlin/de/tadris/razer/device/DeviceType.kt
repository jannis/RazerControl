package de.tadris.razer.device

enum class DeviceType {
    MOUSE,
    KEYBOARD,
}