# RazerControl (WIP)

RazerControl is a simple Java library written in Kotlin for controlling Razer devices via [OpenRazer](https://openrazer.github.io/).
It writes and reads directly from/into the driver files.

Supported OS:

- [x] Linux
- [ ] Windows

Limited device support for:

- [x] Mouse
- [x] Keyboard

...

## Using this in your project

You can add this as a maven/gradle dependency:

see <https://codeberg.org/jannis/RazerControl/packages>

## Mouse Example

```kotlin
val api = RazerAPI()
val mouse = api.mouse() ?: return

println("Connected to $mouse")

mouse.setBrightness(120) // set Brightness to 120/255
mouse.setStatic(0x0000ff) // set static color
mouse.setSpectrum() // enable spectrum effect
mouse.freeSpin = true // enable mouse wheel free spin
```

## Keyboard Example

```kotlin
val api = RazerAPI()
val keyboard = api.keyboard() ?: return

keyboard.gameLedState = true
    
val matrix = KeyboardMatrix(keyboard)
matrix.write(4, 4, 0xffffff) // Set key at 4, 4 to show white color
matrix.flush() // Flush matrix changes
```

## Effect Example

```kotlin
val api = RazerAPI()
val manager = EffectManager(api)

manager.add(StaticEffect(MatrixLocation(3, 3), 0xffffff)) // Permanently turn on key at 3, 3
manager.add(BlinkingEffect(MatrixLocation(5, 3), 0xffffff, 500)) // Blink key at 5, 3 at a 0.5s interval
manager.add(FlashEffect(0xff0000, 1000)) // Flash the whole keyboard red once for 1s

while (true){ 
    // Frequently call tick
    manager.tick()
    Thread.sleep(10)
}
```

## Build Setup

1. clone the repository
2. copy the file `local.sample.gradle` to `local.gradle`

## License

Copyright (C) 2022 Jannis Scheibe <jannis@tadris.de>

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
    
      http://www.apache.org/licenses/LICENSE-2.0
    
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.